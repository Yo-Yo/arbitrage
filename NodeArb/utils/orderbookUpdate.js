/**
 * Created by Alex on 2015/11/18.
 */

var async = require('async');
var https = require('https');

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

var amounts = [0.1, 0.5, 1, 2, 5, 10];

module.exports = {
    startCalculating: function (timeout) {
        console.log("[x] Orderbook correlation started.");
        beginCorrelationLoop(timeout)
    }
};
function beginCorrelationLoop(timeout) {
    console.log("[x] Running orderbook correlation.");
    async.auto({
        getExchangeDollar: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/USD/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getExchangeYuan: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/CNY/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getBitx: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'BITX_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, 1)
            });
        }],
        getIce3x: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'ICE3X_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, 1)
            });
        }],
        getBitfinex: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'BITFINEX_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, rate.getExchangeDollar)
            });
        }],
        getBitstamp: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'BITSTAMP_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, rate.getExchangeDollar)
            });
        }],
        getKraken: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'KRAKEN_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, rate.getExchangeDollar)
            });
        }],
        getBtcc: ['getExchangeYuan', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: 'BTCC_ORDERBOOK'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var asks = hits[0]._source.asks;
                var bids = hits[0]._source.bids;
                var exchange = hits[0]._source.type;
                callback(null, asks, bids, exchange, rate.getExchangeYuan)
            });
        }],
        correlateResults: ['getBitx', 'getIce3x', 'getBitfinex', 'getBitstamp', 'getBtcc', 'getKraken', function (callback, results) {
            var amountRecord = [];
            var exchanges = [results.getBitx, results.getIce3x, results.getBitfinex, results.getBitstamp, results.getBtcc, results.getKraken];
            for(var i = 0; i < amounts.length; i++){
                var exchangeList = [];
                for(var j = 0; j < exchanges.length; j++ ) {
                    exchangeList.push([[j, amounts[i], 0], [j, amounts[i], 0]]);
                }
                amountRecord.push(exchangeList);
            }

            for(var i = 0; i < amountRecord.length; i++){
                for(var j = 0; j < exchanges.length; j++ ){
                    var k = 0;
                    while(true){
                        if(k+1 > exchanges[j][0].length){
                            console.log("[X] Unable to fulfill ask order on exchange " + exchanges[j][2] + " for amount " + amounts[i]);
                            amountRecord[i][j][0][2] = 0;
                            break;
                        }
                        var ask = exchanges[j][0][k++];
                        if(amountRecord[i][j][0][1] > ask[1]){
                            amountRecord[i][j][0][1] -= ask[1];
                            amountRecord[i][j][0][2] += ask[1]*(ask[0] * exchanges[j][3]);
                        }
                        else{
                            amountRecord[i][j][0][2] += amountRecord[i][j][0][1]*(ask[0] * exchanges[j][3]);
                            break;
                        }
                    }
                    k = 0;
                    while(true){
                        if(k+1 > exchanges[j][1].length){
                            console.log("[X] Unable to fulfill bid order on exchange " + exchanges[j][2] + " for amount " + amounts[i]);
                            amountRecord[i][j][1][2] = 0;
                            break;
                        }
                        var bid = exchanges[j][1][k++];
                        if(amountRecord[i][j][1][1] > bid[1]){
                            amountRecord[i][j][1][1] -= bid[1];
                            amountRecord[i][j][1][2] += bid[1]*(bid[0] * exchanges[j][3]);
                        }
                        else{
                            amountRecord[i][j][1][2] += amountRecord[i][j][1][1]*(bid[0] * exchanges[j][3]);
                            break;
                        }
                    }
                }
            }

            for (var i = 0; i < amounts.length; i++){
                var asks = [];
                var bids = [];
                for (var j = 0; j < amountRecord[i].length; j++){
                    asks.push([exchanges[j][2].split("_")[0], amountRecord[i][j][0][2]]);
                    bids.push([exchanges[j][2].split("_")[0], amountRecord[i][j][1][2]]);
                }
                client.index({
                    index: 'arbitrage',
                    type: 'post',
                    body: {
                        type: amounts[i] + "_ORDERBOOK_AMOUNTS",
                        asks: asks,
                        bids: bids,
                        timestamp: Date.now()
                    }
                }, function (err, resp) {
                    if(err)
                        console.error(">Unable to save to elastic for BITSTAMP Percentages - " + err)
                });
            }

        }]
    });

    console.log("[x] Orderbook correlation complete.");
    setTimeout(beginCorrelationLoop, timeout, timeout);
}