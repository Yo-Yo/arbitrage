/**
 * Created by Alex on 2015/11/16.
 */
var express = require('express');
var async = require('async');

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

var router = express.Router();

router.get('/cost', function(req, res, next){
    async.auto({
        get0_1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get0_5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get2: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '2_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get10: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '10_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        correlateResults: ['get0_1', 'get0_5', 'get1', 'get2', 'get5', 'get10', function (callback, results) {
            res.render('comprehensiveCost', {
                results0_1: results.get0_1,
                results0_5: results.get0_5,
                results1: results.get1,
                results2: results.get2,
                results5: results.get5,
                results10: results.get10})
        }]
    });
});

router.get('/costpercoin', function(req, res, next){
    async.auto({
        get0_1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get0_5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get2: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '2_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get10: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '10_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        correlateResults: ['get0_1', 'get0_5', 'get1', 'get2', 'get5', 'get10', function (callback, results) {
            res.render('comprehensiveCostPerBitcoin', {
                results0_1: results.get0_1,
                results0_5: results.get0_5,
                results1: results.get1,
                results2: results.get2,
                results5: results.get5,
                results10: results.get10})
        }]
    });
});

router.get('/percentage', function(req, res, next){
    async.auto({
        get0_1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get0_5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '0.5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get1: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '1_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get2: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '2_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get5: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '5_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        get10: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            type: '10_ORDERBOOK_AMOUNTS'
                        }
                    },
                    sort: [{
                        _timestamp: {
                            order: "desc"
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits[0]._source;
                callback(null, hits.asks, hits.bids)
            });
        }],
        correlateResults: ['get0_1', 'get0_5', 'get1', 'get2', 'get5', 'get10', function (callback, results) {
            res.render('comprehensivePercentage', {
                results0_1: results.get0_1,
                results0_5: results.get0_5,
                results1: results.get1,
                results2: results.get2,
                results5: results.get5,
                results10: results.get10})
        }]
    });
});

module.exports = router;

