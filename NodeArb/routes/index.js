var express = require('express');
var async = require('async');
var https = require('https');

var router = express.Router();

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

router.get('/', function(req, res, next){
    res.render('home')
});

/* GET home page. */
router.get('/overview', function (req, res, next) {
    var date = Date.now() - 6 * 60 * 60 * 1000;
    async.auto({
        getExchangeDollar: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/USD/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getExchangeYuan: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/CNY/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getBitstamp: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'BITSTAMP'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask * rate.getExchangeDollar]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid * rate.getExchangeDollar]);
                }
                callback(null, ask, bid)
            });
        }],
        getBitx: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'BITX'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid]);
                }
                callback(null, ask, bid)
            });
        }],
        getIce3x: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'ICE3X'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid]);
                }
                callback(null, ask, bid)
            });
        }],
        getBitfinex: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'BITFINEX'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask * rate.getExchangeDollar]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid * rate.getExchangeDollar]);
                }
                callback(null, ask, bid)
            });
        }],
        getBtcc: ['getExchangeYuan', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'BTCC'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask * rate.getExchangeYuan]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid * rate.getExchangeYuan]);
                }
                callback(null, ask, bid)
            });
        }],
        getKraken: ['getExchangeDollar', function (callback, rate) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            exchange: 'KRAKEN'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = [];
                var bid = [];
                for (var i = 0; i < hits.length; i++) {
                    ask.push([hits[i]._source.timestamp, hits[i]._source.ask * rate.getExchangeDollar]);
                    bid.push([hits[i]._source.timestamp, hits[i]._source.bid * rate.getExchangeDollar]);
                }
                callback(null, ask, bid)
            });
        }],
        renderResults: ['getBitstamp', 'getBitx', 'getIce3x', 'getBitfinex', 'getBtcc', 'getKraken', function (callback, results) {
            res.render('overview', {
                bitstampask: results.getBitstamp[0], bitstampbid: results.getBitstamp[1],
                bitxask: results.getBitx[0], bitxbid: results.getBitx[1],
                iceask: results.getIce3x[0], icebid: results.getIce3x[1],
                bitfinexask: results.getBitfinex[0], bitfinexbid: results.getBitfinex[1],
                btceask: results.getBtcc[0], btcebid: results.getBtcc[1],
                krakenask: results.getKraken[0], krakenbid: results.getKraken[1]
            });
        }]
    });
});

router.get('/percentage', function (req, res, next) {

    var date = Date.now() - .5 * 60 * 60 * 1000;
    async.auto({
        getBitstamp: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            type: 'bitstampPercentage'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var values = [];
                for (var i = 0; i < hits.length; i++) {
                    values.push([
                        hits[i]._source.timestamp,
                        hits[i]._source.bitstampAsk_bitxBid,
                        hits[i]._source.bitstampBid_bitxAsk,
                        hits[i]._source.bitstampAsk_ice3xBid,
                        hits[i]._source.bitstampBid_ice3xAsk
                    ]);
                }
                callback(null, values)
            });
        }],
        getBitfinex: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            type: 'bitfinexPercentage'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var values = [];
                for (var i = 0; i < hits.length; i++) {
                    values.push([
                        hits[i]._source.timestamp,
                        hits[i]._source.bitfinexAsk_bitxBid,
                        hits[i]._source.bitfinexBid_bitxAsk,
                        hits[i]._source.bitfinexAsk_ice3xBid,
                        hits[i]._source.bitfinexBid_ice3xAsk
                    ]);
                }
                callback(null, values)
            });
        }],
        getBtcc: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            type: 'btccPercentage'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var values = [];
                for (var i = 0; i < hits.length; i++) {
                    values.push([
                        hits[i]._source.timestamp,
                        hits[i]._source.btccAsk_bitxBid,
                        hits[i]._source.btccBid_bitxAsk,
                        hits[i]._source.btccAsk_ice3xBid,
                        hits[i]._source.btccBid_ice3xAsk
                    ]);
                }
                callback(null, values)
            });
        }],
        getKraken: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1000000,
                body: {
                    query: {
                        match: {
                            type: 'krakenPercentage'
                        }
                    },
                    sort: {
                        timestamp: {
                            order: "asc",
                            ignore_unmapped: true
                        }
                    },
                    filter: {
                        range: {
                            timestamp: {
                                gte: date
                            }
                        }
                    }
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var values = [];
                for (var i = 0; i < hits.length; i++) {
                    values.push([
                        hits[i]._source.timestamp,
                        hits[i]._source.krakenAsk_bitxBid,
                        hits[i]._source.krakenBid_bitxAsk,
                        hits[i]._source.krakenAsk_ice3xBid,
                        hits[i]._source.krakenBid_ice3xAsk
                    ]);
                }
                callback(null, values)
            });
        }],
        renderResults: ['getBitstamp', 'getBitfinex', 'getBtcc', 'getKraken', function (callback, results) {
            res.render('percentage', {
                bitstamp: results.getBitstamp,
                bitfinex: results.getBitfinex,
                btcc: results.getBtcc,
                kraken: results.getKraken
            });
        }]
    });

});

module.exports = router;
