/**
 * Created by Alex on 2015/11/14.
 */

var BitX = require('bitx');
var https = require('https');
var bitx = new BitX();
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

module.exports = {
    startMonitoring: function (timeout) {
        console.log("[x] Bitx monitoring starting.");
        beginMonitorLoop(timeout)
    }
};

function beginMonitorLoop(timeout) {
    bitx.getTicker(function(err, ticker) {
        if(err)
            console.error(">Error loading ticker for Bitx - " + err);
        else {
            console.log("[BITX] " + Date.now() + ", ask - R" + ticker.ask + ", bid - R" + ticker.bid);
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    exchange: "BITX",
                    timestamp: Date.now(),
                    ask: parseFloat(ticker.ask),
                    bid: parseFloat(ticker.bid),
                    last_trade: parseFloat(ticker.last_trade),
                    rolling_24_hour_volume: parseFloat(ticker.rolling_24_hour_volume)
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for Bitx - " + err)
            });
        }
    });

    getOrderBooks();
    setTimeout(beginMonitorLoop, timeout, timeout);
}


function getOrderBooks() {
    var options = {
        host : 'api.mybitx.com',
        port : 443,
        path : '/api/1/orderbook?pair=XBTZAR',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        var result = "";
        res.on('data', function (data) {
            result += new Buffer(data).toString('ascii');
        });
        res.on('end', function(){
            try{
            var final = JSON.parse(result);
            var asks = [];
            var bids = [];
            for(var i = 0; i < final.asks.length; i++){
                asks.push([parseFloat(final.asks[i].price).toFixed(10), parseFloat(final.asks[i].volume).toFixed(10)])
            }
            for(var i = 0; i < final.bids.length; i++){
                bids.push([parseFloat(final.bids[i].price).toFixed(10), parseFloat(final.bids[i].volume).toFixed(10)])
            }
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "BITX_ORDERBOOK",
                    timestamp: Date.now(),
                    asks: asks,
                    bids: bids
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BITX_ORDERBOOK - " + err);
            });
            }
            catch(exception){
                console.error("> Unable to parse json for BITX - " + exception)
            }
        });
    });

    req.end();

    req.on('error', function(err) {
        console.error(">Error loading order books for BITX - " + err);
    });
}