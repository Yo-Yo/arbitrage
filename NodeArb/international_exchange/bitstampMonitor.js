/**
 * Created by Alex on 2015/11/14.
 */

var https = require('https');
var async = require('async');
var https = require('https');

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

module.exports = {
    startMonitoring: function (timeout) {
        console.log("[x] Bitstamp monitoring starting.");
        beginMonitorLoop(timeout)
    }
};

function beginMonitorLoop(timeout) {
    var ask;
    var bid;
    var options = {
        host : 'www.bitstamp.net',
        port : 443,
        path : '/api/ticker/',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
            var results = new Buffer(data).toString('ascii');
            var jsonResults = JSON.parse(results);
            ask = jsonResults.ask;
            bid = jsonResults.bid;
            correlateDifferences(ask, bid);
            console.log("[BITSTAMP] " + Date.now() + ", ask - $" + jsonResults.ask + ", bid - $" + jsonResults.bid);
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    exchange: "BITSTAMP",
                    timestamp: Date.now(),
                    ask: parseFloat(jsonResults.ask),
                    bid: parseFloat(jsonResults.bid),
                    last_price: parseFloat(jsonResults.last)
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BITSTAMP - " + err)
            });
        });
    });
    req.end();

    req.on('error', function(err) {
        console.error(">Error loading ticker for BITSTAMP - " + err);
    });

    getOrderBooks();
    setTimeout(beginMonitorLoop, timeout, timeout);
}

function correlateDifferences(ask, bid){
    async.auto({
        getExchangeDollar: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/USD/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getBitx: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'BITX'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        getIce3x: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'ICE3X'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        correlateResults: ['getBitx', 'getIce3x', 'getExchangeDollar', function (callback, results) {
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "bitstampPercentage",
                    bitstampAsk_bitxBid: (((results.getBitx[1]-(ask*results.getExchangeDollar))/(ask*results.getExchangeDollar))*100),
                    bitstampBid_bitxAsk: ((((bid*results.getExchangeDollar)-results.getBitx[0])/results.getBitx[0])*100),
                    bitstampAsk_ice3xBid: (((results.getIce3x[1]-(ask*results.getExchangeDollar))/(ask*results.getExchangeDollar))*100),
                    bitstampBid_ice3xAsk: ((((bid*results.getExchangeDollar)-results.getIce3x[0])/results.getIce3x[0])*100),
                    timestamp: Date.now()
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BITSTAMP Percentages - " + err)
            });
        }]
    });
}

function getOrderBooks() {
    var options = {
        host : 'www.bitstamp.net',
        port : 443,
        path : '/api/order_book/',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        var result = "";
        res.on('data', function (data) {
            result += new Buffer(data).toString('ascii');
        });
        res.on('end', function(){
            try {
                var final = JSON.parse(result);
                var asks = [];
                var bids = [];
                for (var i = 0; i < final.asks.length; i++) {
                    asks.push([parseFloat(final.asks[i][0]).toFixed(10), parseFloat(final.asks[i][1]).toFixed(10)])
                }
                for (var i = 0; i < final.bids.length; i++) {
                    bids.push([parseFloat(final.bids[i][0]).toFixed(10), parseFloat(final.bids[i][1]).toFixed(10)])
                }
                client.index({
                    index: 'arbitrage',
                    type: 'post',
                    body: {
                        type: "BITSTAMP_ORDERBOOK",
                        timestamp: Date.now(),
                        asks: asks,
                        bids: bids
                    }
                }, function (err, resp) {
                    if (err)
                        console.error(">Unable to save to elastic for BITSTAMP_ORDERBOOK - " + err);
                });
            }
            catch(exception){
                console.error("> Unable to parse json for BITSTAMP - " + exception)
            }
        });
    });

    req.end();

    req.on('error', function(err) {
        console.error(">Error loading order books for BITSTAMP - " + err);
    });
}