/**
 * Created by Alex on 2015/11/14.
 */

var async = require('async');
var https = require('https');

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

module.exports = {
    startMonitoring: function (timeout) {
        console.log("[x] BTCC monitoring starting.");
        beginMonitorLoop(timeout)
    }
};

function beginMonitorLoop(timeout) {
    var ask;
    var bid;
    var options = {
        host : 'data.btcchina.com',
        port : 443,
        path : '/data/ticker?market=btccny',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
            var results = new Buffer(data).toString('ascii');
            var jsonResults = JSON.parse(results);
            ask = jsonResults.ticker.buy;
            bid = jsonResults.ticker.sell;

            correlateDifferences(ask, bid);
            console.log("[BTCC] " + Date.now() + ", ask - $" + jsonResults.ticker.buy + ", bid - $" + jsonResults.ticker.sell);
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    exchange: "BTCC",
                    timestamp: Date.now(),
                    ask: parseFloat(jsonResults.ticker.buy),
                    bid: parseFloat(jsonResults.ticker.sell),
                    last_price: parseFloat(jsonResults.ticker.last),
                    volume: parseFloat(jsonResults.ticker.vol)
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BTCC - " + err)
            });
        });
    });
    req.end();

    req.on('error', function(err) {
        console.error(">Error loading ticker for BTCC - " + err);
    });

    getOrderBooks();
    setTimeout(beginMonitorLoop, timeout, timeout);
}

function correlateDifferences(ask, bid){
    async.auto({
        getExchangeYuan: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/CNY/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getBitx: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'BITX'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        getIce3x: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'ICE3X'
                        }
                    },
                    size: 1,
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        correlateResults: ['getBitx', 'getIce3x', 'getExchangeYuan', function (callback, results) {
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "btccPercentage",
                    btccAsk_bitxBid: (((results.getBitx[1]-(ask*results.getExchangeYuan))/(ask*results.getExchangeYuan))*100),
                    btccBid_bitxAsk: ((((bid*results.getExchangeYuan)-results.getBitx[0])/results.getBitx[0])*100),
                    btccAsk_ice3xBid: (((results.getIce3x[1]-(ask*results.getExchangeYuan))/(ask*results.getExchangeYuan))*100),
                    btccBid_ice3xAsk: ((((bid*results.getExchangeYuan)-results.getIce3x[0])/results.getIce3x[0])*100),
                    timestamp: Date.now()
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BTCC Percentages - " + err)
            });
        }]
    });
}

function getOrderBooks() {
    var options = {
        host : 'data.btcchina.com',
        port : 443,
        path : '/data/orderbook?market=btccny',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        var result = "";
        res.on('data', function (data) {
            result += new Buffer(data).toString('ascii');
        });
        res.on('end', function(){
            try{
            var final = JSON.parse(result);
            var asks = [];
            var bids = [];
            for(var i = final.asks.length-1; i > 0; i--){
                asks.push([parseFloat(final.asks[i][0]).toFixed(10), parseFloat(final.asks[i][1]).toFixed(10)])
            }
            for(var i = 0; i < final.bids.length; i++){
                bids.push([parseFloat(final.bids[i][0]).toFixed(10), parseFloat(final.bids[i][1]).toFixed(10)])
            }
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "BTCC_ORDERBOOK",
                    timestamp: Date.now(),
                    asks: asks,
                    bids: bids
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BTCC_ORDERBOOK - " + err);
            });
            }
            catch(exception){
                console.error("> Unable to parse json for BTCC - " + exception)
            }
        });
    });

    req.end();

    req.on('error', function(err) {
        console.error(">Error loading order books for BTCC - " + err);
    });
}