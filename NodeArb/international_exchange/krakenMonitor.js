/**
 * Created by Alex on 2015/11/14.
 */

var fs = require('fs');
var async = require('async');
var https = require('https');
var request		= require('request');

var KrakenClient = require('kraken-api');
var apiKey = fs.readFileSync('./keys/kraken_key.txt');
var secretKey = fs.readFileSync('Wha./keys/kraken_secret.txt');
var kraken = new KrakenClient(apiKey, secretKey);

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

module.exports = {
    startMonitoring: function (timeout) {
        console.log("[x] Kraken monitoring starting.");
        beginMonitorLoop(timeout)
    }
};

function beginMonitorLoop(timeout) {
    var ask;
    var bid;
    kraken.api('Ticker', {"pair": 'XBTUSD'}, function(err, data) {
        if(err) {
            console.error(">Error loading ticker for Kraken - " + err);
        }
        else {
            console.log("[KRAKEN] " + Date.now() + ", ask - $" + data.result.XXBTZUSD.a[0] + ", bid - $" + data.result.XXBTZUSD.b[0]);
            ask = data.result.XXBTZUSD.a[0];
            bid = data.result.XXBTZUSD.b[0];
            correlateDifferences(ask, bid);
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    exchange: "KRAKEN",
                    timestamp: Date.now(),
                    ask: parseFloat(data.result.XXBTZUSD.a[0]),
                    bid: parseFloat(data.result.XXBTZUSD.b[0]),
                    last_trade_amount: parseFloat(data.result.XXBTZUSD.c[0]),
                    last_trade_volume: parseFloat(data.result.XXBTZUSD.c[1]),
                    number_of_trades_today: parseFloat(data.result.XXBTZUSD.t[0]),
                    trades_in_last_24_hours: parseFloat(data.result.XXBTZUSD.t[1])
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for Kraken - " + err)
            });
        }
    });

    getOrderBooks();
    setTimeout(beginMonitorLoop, timeout, timeout);
}

function correlateDifferences(ask, bid){
    async.auto({
        getExchangeDollar: function (callback) {
            var options = {
                host: 'currency-api.appspot.com',
                port: 443,
                path: '/api/USD/ZAR.json',
                method: 'GET',
                headers: {
                    accept: '*/*'
                }
            };
            var req = https.request(options, function (res) {
                res.on('data', function (data) {
                    var results = new Buffer(data).toString('ascii');
                    var jsonResults = JSON.parse(results);
                    callback(null, jsonResults.rate);
                });
            });
            req.end();

            req.on('error', function (err) {
                console.error(">Error loading exchange rates - " + err);
                callback(null, 0);
            });
        },
        getBitx: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'BITX'
                        }
                    },
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        getIce3x: [function (callback) {
            client.search({
                index: 'arbitrage',
                size: 1,
                body: {
                    query: {
                        match : {
                            exchange: 'ICE3X'
                        }
                    },
                    size: 1,
                    sort: [{
                        timestamp: {
                            order: "desc",
                            missing: "_last",
                            ignore_unmapped: true
                        }
                    }]
                }
            }).then(function (resp) {
                var hits = resp.hits.hits;
                var ask = hits[0]._source.ask;
                var bid = hits[0]._source.bid;
                callback(null, ask, bid)
            });
        }],
        correlateResults: ['getBitx', 'getIce3x', 'getExchangeDollar', function (callback, results) {
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "krakenPercentage",
                    krakenAsk_bitxBid: (((results.getBitx[1]-(ask*results.getExchangeDollar))/(ask*results.getExchangeDollar))*100),
                    krakenBid_bitxAsk: ((((bid*results.getExchangeDollar)-results.getBitx[0])/results.getBitx[0])*100),
                    krakenAsk_ice3xBid: (((results.getIce3x[1]-(ask*results.getExchangeDollar))/(ask*results.getExchangeDollar))*100),
                    krakenBid_ice3xAsk: ((((bid*results.getExchangeDollar)-results.getIce3x[0])/results.getIce3x[0])*100),
                    timestamp: Date.now()
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for BITFINEX Percentages - " + err)
            });
        }]
    });
}


function getOrderBooks() {
    var options = {
        host : 'api.kraken.com',
        port : 443,
        path : '/0/public/Depth?pair=XBTUSD',
        method : 'GET',
        headers: {
            accept: '*/*'
        }
    };
    var req = https.request(options, function(res) {
        var result = "";
        res.on('data', function (data) {
            result += new Buffer(data).toString('ascii');
        });
        res.on('end', function(){
            try{
            var final = JSON.parse(result);
            var asks = [];
            var bids = [];
            for(var i = 0; i < final.result.XXBTZUSD.asks.length; i++){
                asks.push([parseFloat(final.result.XXBTZUSD.asks[i][0]).toFixed(10), parseFloat(final.result.XXBTZUSD.asks[i][1]).toFixed(10)])
            }
            for(var i = 0; i < final.result.XXBTZUSD.bids.length; i++){
                bids.push([parseFloat(final.result.XXBTZUSD.bids[i][0]).toFixed(10), parseFloat(final.result.XXBTZUSD.bids[i][1]).toFixed(10)])
            }
            client.index({
                index: 'arbitrage',
                type: 'post',
                body: {
                    type: "KRAKEN_ORDERBOOK",
                    timestamp: Date.now(),
                    asks: asks,
                    bids: bids
                }
            }, function (err, resp) {
                if(err)
                    console.error(">Unable to save to elastic for KRAKEN_ORDERBOOK - " + err);
            });
            }
            catch(exception){
                console.error("> Unable to parse json for KRAKEN - " + exception)
            }
        });
    });

    req.end();

    req.on('error', function(err) {
        console.error(">Error loading order books for KRAKEN - " + err);
    });
}