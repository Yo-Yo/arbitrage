var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var comprehensivePercentage = require(('./routes/comprehensivePercentage'));

var bitx = require('./local_exchange/bitxMonitor');
//bitx.startMonitoring(60*1000);

var ice3x = require('./local_exchange/ice3xMonitor');
//ice3x.startMonitoring(120*1000); //Recommended timeout = 60 seconds.

//TODO Add monitors on kraken EUR, GBP, JPY, CAD
//kraken also trades LTC which may be interesting for cross currency arbitrage
var kraken = require('./international_exchange/krakenMonitor');
//kraken.startMonitoring(40*1000); //For api limit https://www.kraken.com/help/api

var bitstamp = require('./international_exchange/bitstampMonitor');
//bitstamp.startMonitoring(40 * 1000); //600 request per 10min

var bitfinex = require('./international_exchange/bitfinexMonitor');
//bitfinex.startMonitoring(60*1000);

var btcc = require('./international_exchange/btccMonitor');
//btcc.startMonitoring(60*1000);

var orderbook = require('./utils/orderbookUpdate');
//orderbook.startCalculating(60*1000);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/comprehensive', comprehensivePercentage);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
